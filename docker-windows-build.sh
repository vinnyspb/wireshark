#!/bin/bash -e

docker pull registry.gitlab.com/wireshark/wireshark-containers/mingw-dev
docker run --rm --volume .:/wireshark-build registry.gitlab.com/wireshark/wireshark-containers/mingw-dev /bin/bash -c "cd /wireshark-build/build && mingw64-cmake -G Ninja -DENABLE_CCACHE=Yes -DFETCH_lua=Yes .. && cd .. && ninja && ninja user_guide_html && ninja wireshark_nsis_prep && ninja wireshark_nsis && ls -lah packaging/nsis/wireshark-*.exe"
