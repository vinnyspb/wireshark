#include "config.h"
#include <epan/packet.h>

#include <string>

#include "comfortzone_decoder.h"

#define CZ_PORT 9502

static int proto_cz = -1;

// header
static int hf_cz_destination_type = -1;
static int hf_cz_destination_crc_type = -1;
static int hf_cz_comp1_destination_crc_type = -1;
static int hf_cz_source_type = -1;
static int hf_cz_packet_size_type = -1;
static int hf_cz_cmd_type = -1;
static int hf_cz_register_type = -1;

//packet specific
static int hf_cz_fan_speed_duty_type = -1;

static gint ett_cz = -1;

static const value_string message_types[] = {
    { 'W', "Write command" },
    { 'w', "Write reply" },
    { 'R', "Read command" },
    { 'r', "Read reply" },
    { 0, NULL }
};

static int dissect_cz(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree _U_, void *data _U_)
{
    gint offset = 0;
    const guint8 message_size = tvb_get_guint8(tvb, 10);
    const guint8 message_type = tvb_get_guint8(tvb, 11);

    col_set_str(pinfo->cinfo, COL_PROTOCOL, "Comfortzone");
    /* Clear the info column */
    col_clear(pinfo->cinfo, COL_INFO);

    proto_item *ti = proto_tree_add_item(tree, proto_cz, tvb, 0, -1, ENC_NA);

    proto_tree *cz_tree = proto_item_add_subtree(ti, ett_cz);
    proto_tree_add_item(cz_tree, hf_cz_destination_type, tvb, offset, 4, ENC_BIG_ENDIAN);
    offset += 4;
    proto_tree_add_item(cz_tree, hf_cz_destination_crc_type, tvb, offset, 1, ENC_BIG_ENDIAN);
    offset += 1;
    proto_tree_add_item(cz_tree, hf_cz_comp1_destination_crc_type, tvb, offset, 1, ENC_BIG_ENDIAN);
    offset += 1;
    proto_tree_add_item(cz_tree, hf_cz_source_type, tvb, offset, 4, ENC_BIG_ENDIAN);
    offset += 4;
    proto_tree_add_item(cz_tree, hf_cz_packet_size_type, tvb, offset, 1, ENC_BIG_ENDIAN);
    offset += 1;
    proto_tree_add_item(cz_tree, hf_cz_cmd_type, tvb, offset, 1, ENC_BIG_ENDIAN);
    offset += 1;
    proto_tree_add_item(cz_tree, hf_cz_register_type, tvb, offset, 9, ENC_BIG_ENDIAN);
    offset += 9;

    //TODO: check CRC for both header and the message

    const guint8* raw_ptr = tvb_get_ptr(tvb, 0, message_size);

    czdec::KNOWN_REGISTER* kr_decoder = czdec::get_decoder_for_frame((CZ_PACKET_HEADER *)raw_ptr);
    const std::string reg_name = kr_decoder ? kr_decoder->reg_name : "Unknown register";

    col_add_fstr(pinfo->cinfo, COL_INFO, "%s / %s",
             val_to_str(message_type, message_types, "Unknown (0x%02x)"), reg_name.c_str());
    proto_item_append_text(ti, ", %s, %s",
        val_to_str(message_type, message_types, "Unknown (0x%02x)"), reg_name.c_str());

    if(!kr_decoder) {
        return tvb_captured_length(tvb);
    }

    if(!strcmp("Status C8A", kr_decoder->reg_name) && message_type == 'r') {
        R_REPLY_STATUS_V180_C8A *q = (R_REPLY_STATUS_V180_C8A *)raw_ptr;
        offset += sizeof(q->unknown);
        proto_tree_add_item(cz_tree, hf_cz_fan_speed_duty_type, tvb, offset, 2, ENC_LITTLE_ENDIAN);
        offset += 2;
    }

    return tvb_captured_length(tvb);
}

extern "C"
void proto_register_cz(void)
{
    static hf_register_info hf[] = {
        { &hf_cz_destination_type,
            { "Destination", "comfortzone.dest",
            FT_BYTES, BASE_NONE,
            NULL, 0x0,
            NULL, HFILL }
        },
        { &hf_cz_destination_crc_type,
            { "Destination CRC", "comfortzone.dest_crc",
            FT_UINT8, BASE_HEX,
            NULL, 0x0,
            NULL, HFILL }
        },
        { &hf_cz_comp1_destination_crc_type,
            { "Destination Comp1 CRC", "comfortzone.dest_comp1_crc",
            FT_UINT8, BASE_HEX,
            NULL, 0x0,
            NULL, HFILL }
        },
        { &hf_cz_source_type,
            { "Source", "comfortzone.src",
            FT_BYTES, BASE_NONE,
            NULL, 0x0,
            NULL, HFILL }
        },
        { &hf_cz_packet_size_type,
            { "Packet size", "comfortzone.packet_size",
            FT_UINT8, BASE_DEC,
            NULL, 0x0,
            NULL, HFILL }
        },
        { &hf_cz_cmd_type,
            { "Message type", "comfortzone.msg_type",
            FT_UINT8, BASE_HEX,
            VALS(message_types), 0x0,
            NULL, HFILL }
        },
        { &hf_cz_register_type,
            { "Register", "comfortzone.reg",
            FT_BYTES, BASE_NONE,
            NULL, 0x0,
            NULL, HFILL }
        },
        { &hf_cz_fan_speed_duty_type,
            { "Fan speed duty", "comfortzone.fan_speed_duty",
            FT_UINT16, BASE_DEC,
            NULL, 0x0,
            NULL, HFILL }
        }
    };

    /* Setup protocol subtree array */
    static gint *ett[] = {
        &ett_cz
    };

    proto_cz = proto_register_protocol(
        "Comfortzone Protocol", /* name        */
        "Comfortzone",                   /* short name  */
        "comfortzone"                    /* filter_name */
    );

    proto_register_field_array(proto_cz, hf, array_length(hf));
    proto_register_subtree_array(ett, array_length(ett));
}

extern "C"
void proto_reg_handoff_cz(void)
{
    static dissector_handle_t cz_handle;

    cz_handle = create_dissector_handle(dissect_cz, proto_cz);
    dissector_add_uint("udp.port", CZ_PORT, cz_handle);
}
